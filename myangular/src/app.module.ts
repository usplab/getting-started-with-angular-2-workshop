import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { FirstComponent } from './first-component/first.component';
import { SecondComponent } from './second-component/second.component';
import { ContributorComponent } from './contrib-component/contrib-component';
import { UserComponent } from './user-component/user.component';

@NgModule({
    imports: [ BrowserModule, CommonModule, HttpModule ],
    declarations: [ FirstComponent, SecondComponent, ContributorComponent, UserComponent ],
    bootstrap: [ FirstComponent ]
})
export class AppModule {

}
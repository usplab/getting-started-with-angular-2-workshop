import { Component, ViewChild } from '@angular/core'
import { Http } from '@angular/http';

@Component({
    selector: 'first-comp',
    templateUrl: '/dist/first-component/first.component.html',
    styles: [`
    h1 {
        color: red;
    }
    `]
})
export class FirstComponent {
    userName: any = "Balaji Kannan";
    @ViewChild("name") txtInput;

    //contribs: [{id:1}, {id:2}, {id:3}];
    
    cc=[{name:"usp",imgsrc:'https://source.unsplash.com/random/100x100'},
    {name:"usp1",imgsrc:'https://source.unsplash.com/random/100x100'},
    {name:"usp2",imgsrc:'https://source.unsplash.com/random/100x100'}];

    constructor(private http: Http) {
        //this.http.request('https://jsonplaceholder.typicode.com/users').subscribe(data => {console.log(data.json())});
    }

    btnClicked() {
        console.log(this.txtInput.value);
        this.userName = this.txtInput.nativeElement.value;
        this.cc.push({name:this.userName,imgsrc:"https://source.unsplash.com/random/100x100"});


        
    }
}
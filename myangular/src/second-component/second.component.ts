import { Component, Input } from '@angular/core';

@Component({
    selector: 'second-comp',
    template: '<h1>{{ name }} </h1>',
    styles: [`
    h1 {
        color: green;
    }
    `]
})
export class SecondComponent {
    @Input() name;
}
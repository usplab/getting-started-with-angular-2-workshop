export class User {
    public id: number;
    public name: String;
    public userName: String;
    public email: String;
    public address: Address;
}

export class Address {
    public city: String;
    public street: String;
}
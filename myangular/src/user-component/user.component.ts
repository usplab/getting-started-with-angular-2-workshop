import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { User, Address } from './../user.model';

@Component({
    selector: 'user-comp',
    template: `
    <div *ngFor = "let u of users">
        <p>{{u.name}}</p>
        <p>{{u.email}}</p>
        <p>{{u.id}}</p>
        <p>{{u.address.street}}</p>
        <p>{{u.address.city}}</p>
    </div>
    `
})
export class UserComponent {

    private users: User[] = new Array();
    private user: User;

    constructor(private http: Http) {

        this.http.get('https://jsonplaceholder.typicode.com/users').subscribe(data => this.parseUsers(data.json()));
        

    }

    parseUsers(data: any) {
        console.log(data);

        for(let i=0; i < data.length; i++) {
            this.user = new User();
            this.user.id = data[i].id;
            this.user.name = data[i].name;
            this.user.email = data[i].email;
            this.user.userName = data[i].username;
    
            let address: Address = new Address();
            address.city = data[i].address.city;
            address.street = data[i].address.street;
    
            this.user.address = address;
    
            this.users.push(this.user);
        }


        

        console.log(this.users);
    }
}
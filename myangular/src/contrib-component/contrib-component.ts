import { Component, Input } from '@angular/core';

@Component({
    selector: 'contrib-comp',
    template: `
    <div style="border:solid black 2px;max-width:8%">
    <div>
        <img alt="mgechev" src="{{contrib.imgsrc}}" width="117" style="max-width:100%;">
    </div>
    <div style="text-align:center">
        <a href="https://github.com/mgechev">{{contrib.name}}</a>
    </div>
</div>
    `
})
export class ContributorComponent {
    @Input() contrib;
    
}
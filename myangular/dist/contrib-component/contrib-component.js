"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ContributorComponent = (function () {
    function ContributorComponent() {
    }
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ContributorComponent.prototype, "contrib", void 0);
    ContributorComponent = __decorate([
        core_1.Component({
            selector: 'contrib-comp',
            template: "\n    <div style=\"border:solid black 2px;max-width:8%\">\n    <div>\n        <img alt=\"mgechev\" src=\"{{contrib.imgsrc}}\" width=\"117\" style=\"max-width:100%;\">\n    </div>\n    <div style=\"text-align:center\">\n        <a href=\"https://github.com/mgechev\">{{contrib.name}}</a>\n    </div>\n</div>\n    "
        })
    ], ContributorComponent);
    return ContributorComponent;
}());
exports.ContributorComponent = ContributorComponent;
//# sourceMappingURL=contrib-component.js.map
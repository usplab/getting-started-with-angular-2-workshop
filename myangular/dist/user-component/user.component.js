"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var user_model_1 = require("./../user.model");
var UserComponent = (function () {
    function UserComponent(http) {
        var _this = this;
        this.http = http;
        this.users = new Array();
        this.http.get('https://jsonplaceholder.typicode.com/users').subscribe(function (data) { return _this.parseUsers(data.json()); });
    }
    UserComponent.prototype.parseUsers = function (data) {
        console.log(data);
        for (var i = 0; i < data.length; i++) {
            this.user = new user_model_1.User();
            this.user.id = data[i].id;
            this.user.name = data[i].name;
            this.user.email = data[i].email;
            this.user.userName = data[i].username;
            var address = new user_model_1.Address();
            address.city = data[i].address.city;
            address.street = data[i].address.street;
            this.user.address = address;
            this.users.push(this.user);
        }
        console.log(this.users);
    };
    UserComponent = __decorate([
        core_1.Component({
            selector: 'user-comp',
            template: "\n    <div *ngFor = \"let u of users\">\n        <p>{{u.name}}</p>\n        <p>{{u.email}}</p>\n        <p>{{u.id}}</p>\n        <p>{{u.address.street}}</p>\n        <p>{{u.address.city}}</p>\n    </div>\n    "
        }),
        __metadata("design:paramtypes", [http_1.Http])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map
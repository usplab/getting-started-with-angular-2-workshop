"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var FirstComponent = (function () {
    function FirstComponent(http) {
        this.http = http;
        this.userName = "Balaji Kannan";
        //contribs: [{id:1}, {id:2}, {id:3}];
        this.cc = [{ name: "usp", imgsrc: 'https://source.unsplash.com/random/100x100' },
            { name: "usp1", imgsrc: 'https://source.unsplash.com/random/100x100' },
            { name: "usp2", imgsrc: 'https://source.unsplash.com/random/100x100' }];
        //this.http.request('https://jsonplaceholder.typicode.com/users').subscribe(data => {console.log(data.json())});
    }
    FirstComponent.prototype.btnClicked = function () {
        console.log(this.txtInput.value);
        this.userName = this.txtInput.nativeElement.value;
        this.cc.push({ name: this.userName, imgsrc: "https://source.unsplash.com/random/100x100" });
    };
    __decorate([
        core_1.ViewChild("name"),
        __metadata("design:type", Object)
    ], FirstComponent.prototype, "txtInput", void 0);
    FirstComponent = __decorate([
        core_1.Component({
            selector: 'first-comp',
            templateUrl: '/dist/first-component/first.component.html',
            styles: ["\n    h1 {\n        color: red;\n    }\n    "]
        }),
        __metadata("design:paramtypes", [http_1.Http])
    ], FirstComponent);
    return FirstComponent;
}());
exports.FirstComponent = FirstComponent;
//# sourceMappingURL=first.component.js.map